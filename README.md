# nextcloud-server

A containerised [Nextcloud](https://nextcloud.com) server.

## Getting Started

*WARNING: Do not use a directory mount in Windows to store the data if using WSL2 for the Docker backend. Crossing the filesystem boundary may cause IO errors when reading and writing chunks.*

Launch the containers using the provided `docker-compose.yml` file with the following command:

```
$ docker-compose up -d
```

If restoring from a backup use the provided script to restore the data and launch the containers:

```
$ sh nextcloud-server-backup.sh -r -p <PATH>
```

## Configuration

Environment variables can be passed to the containers via the `config/web.env` and `config/db.env` files.

## Usage

Launch the container using the provided `docker-compose.yml` file with the following command:

```
$ docker-compose up -d
```

## Backups

The `scripts/nextcloud-server-backup.sh` script can be used to create and restore incremental backups to and from a specified directory.
