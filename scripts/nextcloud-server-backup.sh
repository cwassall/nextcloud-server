#!/usr/bin/env bash

Help()
{
   echo "Create or restore a Nextcloud server backup."
   echo
   echo "Syntax: nextcloud-server-backup [-b|r|l|n|h]"
   echo "-n PATH    The path to the directory used to store backups."
   echo "-b         Create a backup."
   echo "-r         Restore a backup."
   echo "-l         Include Let's Encrypt data."
   echo "-h         Print this Help."
   echo
}

BACKUP_PATH=
while getopts ":hlbrn:" option; do
    case $option in
        h)
            Help
            exit;;
        l)
            LETSENCRYPT=1;;
        b)
            BACKUP=1;;
        r)
            RESTORE=1;;
        n)
            BACKUP_PATH=$OPTARG;;
        \?)
            Help
            echo "Error: Invalid option"
            exit;;
    esac
done

if [ ! "$BACKUP_PATH" ]
then
    Help
    echo "Error: Invalid path"
    exit
fi

if [ ! "$BACKUP" ] && [ ! "$RESTORE" ]
then
    Help
    echo "Error: A backup must be created or restored"
    exit
fi
echo "path $BACKUP_PATH"
exit

echo "Pulling rsync..."
docker pull eeacms/rsync

echo "Stopping containers..."
cd /usr/local/src/nextcloud-server
docker-compose down

if [ "$BACKUP" ]
then
    echo "Creating Nextcloud backup..."
    if [ "$LETSENCRYPT" ]
    then
        docker run --rm \
            -v $BACKUP_PATH:/backup \
            -v nextcloud-server_db:/data/nextcloud-server_db \
            -v nextcloud-server_nextcloud:/data/nextcloud-server_nextcloud \
            -v nextcloud-server_apps:/data/nextcloud-server_apps \
            -v nextcloud-server_config:/data/nextcloud-server_config \
            -v nextcloud-server_data:/data/nextcloud-server_data \
            -v nextcloud-server_acme:/data/nextcloud-server_acme \
            -v nextcloud-server_certs:/data/nextcloud-server_certs \
            -v nextcloud-server_vhost.d:/data/nextcloud-server_vhost.d \
            -v nextcloud-server_html:/data/nextcloud-server_html \
            eeacms/rsync sh -c " \
            rsync -Aavx --delete /data/nextcloud-server_db/ /backup/nextcloud-server_db/ && \
            rsync -Aavx --delete /data/nextcloud-server_nextcloud/ /backup/nextcloud-server_nextcloud/ && \
            rsync -Aavx --delete /data/nextcloud-server_apps/ /backup/nextcloud-server_apps/ && \
            rsync -Aavx --delete /data/nextcloud-server_config/ /backup/nextcloud-server_config/ && \
            rsync -Aavx --delete /data/nextcloud-server_data/ /backup/nextcloud-server_data/ && \
            rsync -Aavx --delete /data/nextcloud-server_acme/ /backup/nextcloud-server_acme/ && \
            rsync -Aavx --delete /data/nextcloud-server_certs/ /backup/nextcloud-server_certs/ && \
            rsync -Aavx --delete /data/nextcloud-server_vhost.d/ /backup/nextcloud-server_vhost.d/ && \
            rsync -Aavx --delete /data/nextcloud-server_html/ /backup/nextcloud-server_html/"
    else
        docker run --rm \
            -v $BACKUP_PATH:/backup \
            -v nextcloud-server_db:/data/nextcloud-server_db \
            -v nextcloud-server_nextcloud:/data/nextcloud-server_nextcloud \
            -v nextcloud-server_apps:/data/nextcloud-server_apps \
            -v nextcloud-server_config:/data/nextcloud-server_config \
            -v nextcloud-server_data:/data/nextcloud-server_data \
            eeacms/rsync sh -c " \
            rsync -Aavx --delete /data/nextcloud-server_db/ /backup/nextcloud-server_db/ && \
            rsync -Aavx --delete /data/nextcloud-server_nextcloud/ /backup/nextcloud-server_nextcloud/ && \
            rsync -Aavx --delete /data/nextcloud-server_apps/ /backup/nextcloud-server_apps/ && \
            rsync -Aavx --delete /data/nextcloud-server_config/ /backup/nextcloud-server_config/ && \
            rsync -Aavx --delete /data/nextcloud-server_data/ /backup/nextcloud-server_data/"
    fi
else
    echo "Ensuring volumes exist..."
    docker volume create nextcloud-server_db
    docker volume create nextcloud-server_nextcloud
    docker volume create nextcloud-server_apps
    docker volume create nextcloud-server_config
    docker volume create nextcloud-server_data

    if [ "$LETSENCRYPT" ]
    then
        docker volume create nextcloud-server_acme
        docker volume create nextcloud-server_certs
        docker volume create nextcloud-server_vhost.d
        docker volume create nextcloud-server_html
    fi

    echo "Restoring Nextcloud backup..."
    if [ "$LETSENCRYPT" ]
    then
        docker run --rm \
            -v $BACKUP_PATH:/backup \
            -v nextcloud-server_db:/data/nextcloud-server_db \
            -v nextcloud-server_nextcloud:/data/nextcloud-server_nextcloud \
            -v nextcloud-server_apps:/data/nextcloud-server_apps \
            -v nextcloud-server_config:/data/nextcloud-server_config \
            -v nextcloud-server_data:/data/nextcloud-server_data \
            -v nextcloud-server_acme:/data/nextcloud-server_acme \
            -v nextcloud-server_certs:/data/nextcloud-server_certs \
            -v nextcloud-server_vhost.d:/data/nextcloud-server_vhost.d \
            -v nextcloud-server_html:/data/nextcloud-server_html \
            eeacms/rsync sh -c " \
            rsync -Aavx --delete /backup/nextcloud-server_db/ /data/nextcloud-server_db/ && \
            rsync -Aavx --delete /backup/nextcloud-server_nextcloud/ /data/nextcloud-server_nextcloud/ && \
            rsync -Aavx --delete /backup/nextcloud-server_apps/ /data/nextcloud-server_apps/ && \
            rsync -Aavx --delete /backup/nextcloud-server_config/ /data/nextcloud-server_config/ && \
            rsync -Aavx --delete /backup/nextcloud-server_data/ /data/nextcloud-server_data/ && \
            rsync -Aavx --delete /backup/nextcloud-server_acme/ /data/nextcloud-server_acme/ && \
            rsync -Aavx --delete /backup/nextcloud-server_certs/ /data/nextcloud-server_certs/ && \
            rsync -Aavx --delete /backup/nextcloud-server_vhost.d/ /data/nextcloud-server_vhost.d/ && \
            rsync -Aavx --delete /backup/nextcloud-server_html/ /data/nextcloud-server_html/"
    else
        docker run --rm \
            -v $BACKUP_PATH:/backup \
            -v nextcloud-server_db:/data/nextcloud-server_db \
            -v nextcloud-server_nextcloud:/data/nextcloud-server_nextcloud \
            -v nextcloud-server_apps:/data/nextcloud-server_apps \
            -v nextcloud-server_config:/data/nextcloud-server_config \
            -v nextcloud-server_data:/data/nextcloud-server_data \
            eeacms/rsync sh -c " \
            rsync -Aavx --delete /backup/nextcloud-server_db/ /data/nextcloud-server_db/ && \
            rsync -Aavx --delete /backup/nextcloud-server_nextcloud/ /data/nextcloud-server_nextcloud/ && \
            rsync -Aavx --delete /backup/nextcloud-server_apps/ /data/nextcloud-server_apps/ && \
            rsync -Aavx --delete /backup/nextcloud-server_config/ /data/nextcloud-server_config/ && \
            rsync -Aavx --delete /backup/nextcloud-server_data/ /data/nextcloud-server_data/"
    fi
fi

echo "Starting containers..."
docker-compose up -d

if [ "$RESTORE" ]
then
    docker exec -i nextcloud-server_app sh -c 'chown -R www-data:www-data /var/www/html'
fi